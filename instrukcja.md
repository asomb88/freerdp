### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://github.com/FreeRDP/FreeRDP.git
cd FreeRDP

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y freerdp2

cmake .

make -j8

```

### QTCreator Includes
```
include/
/usr/include/winpr2/

```
